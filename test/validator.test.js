const expressValidator = require('../index')

const res = (send) => {
  return {
    status: (code) => {
      return { send }
    }
  }
}
const createValidator = (schema) => {
  const validator = expressValidator(schema)

  return (body) => {
    let req = { body }

    let error, validated

    validator(req, res((json) => {
      error = json
    }), () => {
      validated = req.validated
    })

    return [error, validated]
  }
}



describe("Validator test\n\tschema - body:", () => {
  it('empty body - empty body', () => {
    let validator = createValidator({})

    const [error, validated] = validator({})

    if (error) {
      throw new Error('С пустой схемой должен пропускать')
    }
  })

  it('empty body - { name: string }', () => {
    let validator = createValidator({})

    const [error, validated] = validator({})

    if (error) {
      throw new Error('С пустой схемой должен пропускать')
    }
  })

  it('{ name: string.required } - empty body', () => {
    let validator = createValidator({
      name: {
        type: 'string',
        required: true
      }
    })

    const [error, validated] = validator({})

    if (validated) {
      throw new Error('Не должен пускать с пустым телом')
    }
  })

  it('{ name: string.required } - { name: string }', () => {
    let validator = createValidator({
      name: {
        type: 'string',
        required: true
      }
    })

    const [error, validated] = validator({
      name: 'Donald'
    })

    if (error) {
      throw new Error('Должен пускать с пустым телом')
    }
  })

  it('{ num: number } - { num: string }', () => {

    let validator = createValidator({
      num: 'number'
    })

    const [error, validated] = validator({
      num: 'one'
    })

    if (validated) {
      throw new Error('Не должен пускать с неправильным типом')
    }
  })

  it('{ items: { foo: string.required } } - { items: { goo: string } }', () => {
    let validator = createValidator({
      items: {
        type: 'object',
        required: true,
        data: {
          foo: {
            type: 'string',
            required: true
          }
        }
      }
    })

    const [error, validated] = validator({
      items: {
        goo: 'goo'
      }
    })

    if (validated) {
      throw new Error('Не должен пускать с неправильным типом')
    }
  })

  it('{ items: { foo: string.required }[] } - { items: { goo: string } }', () => {
    let validator = createValidator({
      items: {
        type: 'array',
        required: true,
        data: [{
          foo: {
            type: 'string',
            required: true,
          }
        }]
      }
    })

    const [error, validated] = validator({
      items: {
        goo: 'goo'
      }
    })

    if (validated) {
      throw new Error('Не должен пускать с неправильным типом')
    }
  })

  it('{ phone: 79995550203 } - { phone: 7--5550203 }', () => {
    let validator = createValidator({
      phone: {
        type: 'string',
        required: true,
        template: /^(\+?7|8)\d{10}$/
      }
    })

    const [error, validated] = validator({
      phone: '7--5550203'
    })

    if (validated) {
      throw new Error('Не должен пускать с неправильным шаблоном')
    }
  })



  it('Final', () => {
    let schema = {
      phone: {
        type: 'string',
        required: true,
        template: /^(\+?7|8)\d{10}$/
      },
      items: {
        type: 'array',
        required: true,
        data: [{
          foo: {
            type: 'string',
            required: true,
          }
        }]
      },
      name: {
        type: 'string',
        required: true
      },
      count: 'number'
    }

    let body = {
      name: 'Donald',
      phone: '79995550203',
      items: [{
        foo: '1',
        goo: 'GOO'
      },{
        foo: '2',
        koko: 'KOKO'
      }],
      unsup: 200
    }

    let validator = createValidator(schema)

    const [error, validated] = validator(body)

    if (error) {
      throw new Error('Тело запроса на самом деле правильное')
    }

    console.log('Schema\n', schema)
    console.log('Body\n', body)
    console.log('Error\n', error)
    console.log('Validated\n', validated)
  })
})
