const CustomError = require('@vl-utils/e-error')

/**
 * @typedef {object} ValidatorSchemaTemplate
 * @param {RegExp} regexp
 * @param {string} message
 */
/**
 * Функция проверяет корректность шаблона
 * @param {RegExp} template - шаблон в виде регулярного выражения
 * @param {ValidatorSchemaTemplate} template - шаблон в виде объекта
 * @return {ValidatorSchemaTemplate} 
 */
module.exports = (template) => {
  if (template instanceof RegExp) {
    return {
      regexp: template,
      message: `Неверный формат`
    }
  }

  if (typeof template == 'object' && 'regexp' in template && template.regexp instanceof RegExp) {
    return {
      regexp: template.regexp,
      message: template.message || `Неверный формат`
    }
  }

  throw new CustomError(`Invalid template ${template}`)
}

// TODO: Добавить возможность передавать специфическую функцию для сверения шаблона
// template: schemaKey.template && 'test' in schemaKey.template && typeof schemaKey.template.test == 'function' ? schemaKey.template : undefined,
