const CustomError = require('@vl-utils/e-error')
const checkObject = require('./check-object')

/**
 * @param {object} validating - Объект, который необходимо провалидировать
 * @param {ValidatorSchema} schema - Схема для валидации
 * @returns {object} - Валидированный объект
 */
module.exports = (validating, schema) => {
  if (typeof validating != 'object' || validating === null) {
    throw new CustomError(`validating must be an object`)
  }

  if (typeof schema != 'object' || schema === null) {
    throw new CustomError(`schema must be an object`)
  }

  const [validatedObject, errors] = checkObject(validating, schema)


  if (Object.keys(errors).length) {
    throw new CustomError(errors)
  }
  return validatedObject
}
