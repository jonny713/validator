/**
 * Функция валидации объекта
 * @param {object} validating - валидируемый объект
 * @param {object} schema - схема
 * @param {string} prefix - префикс для имени поля, если объект вложен
 */
module.exports = (validating, schema, prefix = '') => {
  let resObject = {}
  let errors = {}

  for (let key in schema) {
    try {
      // console.log(prefix + key);
      let [ value, fieldErrors ] = checkField(validating, schema, key, prefix)
      if (value !== undefined) {
        resObject[key] = value
      }
      errors = {...errors, ...fieldErrors}

    } catch (e) {
      // console.log('->', e);
      errors[e.parsedMessage.field] = e.parsedMessage.text
    }
  }

  return [ resObject, errors ]
}

// Require перенесен вниз для избежания циклического подключения зависимостей
const checkField = require('./check-field')
