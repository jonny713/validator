const validateTemplate = require('./validate-template')

/**
 * @typedef {object} ValidatorSchemaKey
 * @param {boolean} required - default:false
 * @param {string} type - default:string - enum:string,boolean,number,object,array
 * @param {ValidatorSchemaTemplate[]} templates - default:undefined
 */

 
/**
 * Функция проверяет корректность элемента схемы в соответствие с внутренними правилами
 * @param {string} schemaKey
 * @param {object} schemaKey
 * @return {ValidatorSchemaKey}
 */
module.exports = (schemaKey) => {
  // Если ключ схемы - строка, то это должен быть один из стандартных типов. В противном случае - строка
  if (typeof schemaKey == 'string') {
    return {
      type: ['string', 'boolean', 'number', 'object', 'array'].includes(schemaKey) ?  schemaKey : 'string'
    }
  }

  // Если ключ схемы не строка, он должен быть объектом
  if (typeof schemaKey !== 'object') {
    throw new CustomError(`Invalid schema key ${schemaKey}`)
  }

  // Проверяем и устанавливаем шаблон, если указан
  const { template } = schemaKey
  let templates = []
  if (template !== undefined) {
    if (Array.isArray(template)) {
      templates = template.map(validateTemplate)
    } else {
      templates = [validateTemplate(template)]
    }
  }

  // Возвращаем объект со стандартными полями
  return {
    required: !!schemaKey.required,
    type: ['string', 'boolean', 'number', 'object', 'array'].includes(schemaKey.type) ?  schemaKey.type : 'string',
    templates,
    data: schemaKey.data
  }
}
