const isEmpty = value => value === undefined
    || value === null
    || (typeof value === 'object' && Object.keys(value).length === 0)
    || (typeof value === 'string' && value.trim().length === 0)

const validatorSchemaKey = require('./validator-schema-key')
const CustomError = require('@vl-utils/e-error')
const checkObject = require('./check-object')

/**
 * Функция валидации конкретного поля
 * @param {object} validating - валидируемое поле
 * @param {object} schema - схема
 * @param {string} key - имя поля
 * @param {string} prefix - префикс для имени поля, если объект вложен
 */
module.exports = (validating, schema, key, prefix) => {
  let value = validating[key]

  // Получаем информацию по схеме конкретного поля
  let schemaKey = validatorSchemaKey(schema[key])

  // Если поле required, а значение пустое, возвращаем ошибку
  if (schemaKey.required && isEmpty(value)) {
    throw new CustomError({
      field: prefix + key,
      text: `Не может быть пустым`
    })
  }

  // Если поле НЕ required и значение пустое, возвращаем пустое значение
  if (isEmpty(value)) {
    return [undefined, {}]
  }

  // Проверяем соответствие типа
  if ((typeof value != schemaKey.type && schemaKey.type != 'array') || (schemaKey.type == 'array' && !Array.isArray(value))) {
    throw new CustomError({
      field: prefix + key,
      text: `Неверный тип`
    })
  }

  // Проверяем соответствие шаблону, если тот указан
  if (Array.isArray(schemaKey.templates) && schemaKey.templates.length) {
    schemaKey.templates.forEach(({ regexp, message }) => {
      if (!regexp.test(value)) {
        throw new CustomError({
          field: prefix + key,
          text: message
        })
      }
    })
  }

  // Если поле объект или массив - проверяем внутренние данные
  if (schemaKey.data) {
    if (Array.isArray(schemaKey.data) && Array.isArray(value)) {
      let r = []
      let errors = {}
      for (let i = 0; i < value.length; i++) {
        const [ checked, checkErrors ] = checkObject(value[i], schemaKey.data[0], key + `.${i}.`)
        r.push(checked)
        errors = {...errors, ...checkErrors}
      }
      return [ r, errors ]
    }
    return checkObject(value, schemaKey.data, key + '.')
  }

  // Если все проверки пройдены успешно, возвращаем результат
  return [ value, {} ]
}
