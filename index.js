const validator = require('./lib/validator.js')

module.exports = (schema) => {
  return (req, res, next) => {
    try {
      req.validated = validator(req.body, schema)

      next()
    } catch (e) {
      // console.error(e.parsedMessage || e)

      return res.status(400).send(e.parsedMessage)
    }
  }
}
