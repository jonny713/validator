# Описание
* Предоставляет промежуточный маршрут для express js, валидирующий body в соответствии со схемой

# Подключение
Подключаем модуль к проекту
```shell
npm i @vl-utils/validator
```
Объявляем для дальнейщего использования
```javascript
const expressValidator = require('@vl-tests/validator')
```

# Использование
Вызов функции возвращает стандартный промежуточный маршрут express js
```javascript
const expressValidator = require('@vl-tests/validator')

const express = require('express')
const router = express.Router()

router.post('/api', expressValidator({
  name: {
    required: true,
    type: 'string'
  },
  phone: {
    required: true,
    type: 'string',
    template: /^(\+?7|8)?\d{10}$/
  },
  items: {
    type: 'array',
    required: true,
    data: [{
      quantity: {
        required: true,
        type: 'number'
      },
      itemId: {
        required: true,
        type: 'string'
      }
    }]
  }
}))
```

# Схема
Схема состоит из семантического перечисления всех ожидаемых полей.
В качестве значения может быть строка с типом
```javascript
const schema = {
  name: 'string'
}
```
или объект с набором полей
```javascript
const schema = {
  name: {
    type: 'string'
  }
}
```

Объект, используемый для описания поля
```javascript
/**
 * @typedef {object} SchemaField
 * @param {string} type - требуемый тип
 * @param {boolean} required - необходимость наличия поля
 * @param {RegExp || RegExp[] || SFTemplate || SFTemplate[]} template - шаблон для сверения
 * @param {SchemaField[] || { field: SchemaField }} data - вложенные данные
 */
```

Шаблон может быть описан в виде регулярного выражения (или их массива), а может быть описан объектом с индивидуальным сообщением
```javascript
/**
 * @typedef {object} SFTemplate
 * @param {RegExp} regexp
 * @param {string} message
 */
```
Поля с типом object или array могут иметь описание внутренних данных.

Пример:
```javascript
const schema = {
  items: {
    type: 'array',
    data: [{
      quantity: {
        required: true,
        type: 'number'
      },
      itemId: {
        required: true,
        type: 'string'
      }
    }]
  },
  user: {
    type: 'object',
    data: {
      id: {
        required: true,
        type: 'number'
      },
      pos: 'number'
    }
  }
}
```

# Результат успешной валиадции
В случае успешной валидации, к объекту запроса будет добавлено поле validated, содержащее поля в соответствии со схемой
```javascript
router.post('/api', (req, res) => {
  const { validated } = req
})
```

# Результат неудачной валидации
Если валидация не пройдена, в ответ за запрос будет выдан http статус 400 и объект ошибки, содержащей пары ключ-значения. В качестве ключа - имя поля с ошибкой, в качестве значения - текст ошибки

Пример:
```json
{
  "name": "Неверный формат",
  "user.pos": "Неверный тип"
}
```

# Запланированные обновления
 * Функция-обработчик в качестве шаблона
 * Пользовательский текст всех ошибок
